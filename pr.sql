﻿ALTER SEQUENCE audit_trail_seq START 1;

CREATE TABLE audit_trail (
  id BIGINT CONSTRAINT audit_trail_pk PRIMARY KEY DEFAULT nextval('audit_trail_seq'),
  created_at timestamp with time zone NULL,
  updated_at timestamp with time zone NULL
);

CREATE SEQUENCE user_seq START 1;

CREATE TABLE security_user (
  id BIGINT CONSTRAINT user_seq_pk PRIMARY KEY DEFAULT nextval('user_seq'),
  username VARCHAR(50) NOT NULL UNIQUE,
  password VARCHAR(255) NOT NULL
);


CREATE SEQUENCE role_seq START 1;

CREATE TABLE role (
  id BIGINT CONSTRAINT role_pk PRIMARY KEY DEFAULT nextval('role_seq'),
  authority VARCHAR(50) UNIQUE NOT NULL
);

CREATE TABLE user_role (
  role_id BIGINT NOT NULL
                            CONSTRAINT role_ref
                            REFERENCES role(id)
                            ON UPDATE RESTRICT
                            ON DELETE RESTRICT,
  user_id BIGINT NOT NULL
                            CONSTRAINT user_ref
                            REFERENCES security_user(id)
                            ON UPDATE RESTRICT
                            ON DELETE RESTRICT,
  CONSTRAINT user_role_pkey PRIMARY KEY (role_id, user_id)
);

CREATE SEQUENCE sh_command_seq START 1;

CREATE TABLE sh_command (
  id BIGINT CONSTRAINT sh_command_pk PRIMARY KEY DEFAULT nextval('sh_command_seq'),
  version INT DEFAULT 0 NOT NULL,
  command VARCHAR(1000) NOT NULL UNIQUE,
  audit_trail_id BIGINT NULL REFERENCES audit_trail(id)
);

CREATE SEQUENCE sh_process_log_seq START 1;

CREATE TABLE sh_process_log (
  id BIGINT CONSTRAINT sh_process_log_pk PRIMARY KEY DEFAULT nextval('sh_process_log_seq'),
  text VARCHAR(5000) NULL
);

CREATE SEQUENCE sh_process_seq START 1;

CREATE TABLE sh_process (
  id BIGINT CONSTRAINT sh_process_pk PRIMARY KEY DEFAULT nextval('sh_process_seq'),
  text VARCHAR(5000) NULL,
  process_number INT NOT NULL,
  audit_trail_id BIGINT NULL REFERENCES audit_trail(id),
  user_id BIGINT NULL REFERENCES security_user(id),
  sh_command_id BIGINT NULL REFERENCES sh_command(id),
  version INT DEFAULT 0 NOT NULL,
  is_running BOOLEAN DEFAULT FALSE NOT NULL
);

CREATE TABLE sh_process_err_output (
  sh_process_id BIGINT NOT NULL
    CONSTRAINT sh_process_ref
    REFERENCES sh_process(id)
    ON UPDATE RESTRICT
    ON DELETE RESTRICT,
  err_output_id BIGINT NOT NULL
    CONSTRAINT sh_process_log_ref
    REFERENCES sh_process_log(id)
    ON UPDATE RESTRICT
    ON DELETE RESTRICT,
  CONSTRAINT sh_process_err_output_pkey PRIMARY KEY (sh_process_id, err_output_id)
);

CREATE TABLE sh_process_std_output (
  sh_process_id BIGINT NOT NULL
    CONSTRAINT sh_process_ref
    REFERENCES sh_process(id)
    ON UPDATE RESTRICT
    ON DELETE RESTRICT,
  std_output_id_id BIGINT NOT NULL
    CONSTRAINT sh_process_log_ref
    REFERENCES sh_process_log(id)
    ON UPDATE RESTRICT
    ON DELETE RESTRICT,
  CONSTRAINT sh_process_std_output_pkey PRIMARY KEY (sh_process_id, std_output_id_id)
);


CREATE TABLE sh_command_role (
  sh_command_id BIGINT NOT NULL
    CONSTRAINT sh_command_ref
    REFERENCES sh_command(id)
    ON UPDATE RESTRICT
    ON DELETE RESTRICT,
  role_id BIGINT NOT NULL
    CONSTRAINT role_ref
    REFERENCES role(id)
    ON UPDATE RESTRICT
    ON DELETE RESTRICT,
  CONSTRAINT sh_command_role_role_pkey PRIMARY KEY (sh_command_id, role_id)
);



