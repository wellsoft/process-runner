package processrunner

import com.sun.jna.Pointer
import groovy.util.logging.Slf4j
import processrunner.util.*

@Slf4j
class ProcessUtil {

    public static int getProcessPID(Process process) {
        def pid = -99
        if (process.getClass().getName().equals("java.lang.UNIXProcess")) {
            /* get the PID on unix/linux systems */
            try {
                pid =  process.pid;
            } catch (Throwable e) {
                log.warn("Can`t get process PID on UNIX", e)
            }
        } else  if (process.getClass().getName().equals("java.lang.Win32Process") ||
                process.getClass().getName().equals("java.lang.ProcessImpl")) {
            /* determine the pid on windows plattforms */
            try {
                long handl = process.handle;
                processrunner.util.Kernel32 kernel = processrunner.util.Kernel32.INSTANCE;
                processrunner.util.W32API.HANDLE handle = new processrunner.util.W32API.HANDLE();
                handle.setPointer(Pointer.createConstant(handl));
                pid = kernel.GetProcessId(handle);
            } catch (Throwable e) {
                log.warn("Can`t get process PID on Windows", e)
            }
        }
        return pid
    }
}
