package processrunner.concurrent

import java.util.concurrent.ThreadFactory

/**
 * Created by Grigoriy on 23.11.2015.
 */
class DaemonThreadFactory implements ThreadFactory {
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r);
        thread.setDaemon(true);
        return thread;
    }
}