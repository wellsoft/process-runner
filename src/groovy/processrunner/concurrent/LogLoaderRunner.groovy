package processrunner.concurrent

import processrunner.ShProcess
import processrunner.ShProcessLog

/**
 * Created by Grigoriy on 23.11.2015.
 */

class LogLoaderRunner implements Runnable {
    InputStream stream
    Long shProcessId
    Boolean isErrorStream

    public LogLoaderRunner(def stream, Long shProcessId, Boolean isErrorStream) {
        this.stream = stream
        this.shProcessId = shProcessId
        this.isErrorStream = isErrorStream
    }

    @Override
    void run() {
        try {
            Scanner sc = new Scanner(stream)
            String batch = '';
            def countLine = 0
            while (sc.hasNextLine()) {
                def line = sc.nextLine()
                if (line == '') {
                    continue
                }
                batch = "${batch}\n${line}"
                countLine++
                if (countLine > 3) {
                    ShProcess.withNewSession {
                        ShProcess shProcess = ShProcess.get(shProcessId)
                        ShProcessLog log = new ShProcessLog(text: batch).save()
                        if (isErrorStream) {
                            shProcess.addToErrOutput(log)
                        } else {
                            shProcess.addToStdOutput(log)
                        }
                        shProcess.save(flush: true)
                        batch = ''
                        countLine == 0
                    }
                }
                if (batch != '') {
                    ShProcess.withNewSession {
                        ShProcess shProcess = ShProcess.get(shProcessId)
                        ShProcessLog log = new ShProcessLog(text: batch).save()
                        if (isErrorStream) {
                            shProcess.addToErrOutput(log)
                        } else {
                            shProcess.addToStdOutput(log)
                        }
                        shProcess.save(flush: true)
                        batch = ''
                        countLine == 0
                    }
                }
            }
        }
        catch (IOException e) {
            log.error("ERROR OCCURRED while fetching log of process", e)
        }
    }
}