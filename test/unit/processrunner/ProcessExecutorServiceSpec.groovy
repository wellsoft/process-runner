package processrunner

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

import java.util.concurrent.TimeUnit

@TestFor(ProcessExecutorService)
@Mock([ShCommand, ShProcess, SecurityUser, Role, AuditTrail])
class ProcessExecutorServiceSpec extends Specification {

    ShCommand shCommand
    SpringSecurityService springSecurityServiceMock = Mock(SpringSecurityService)

    def setup() {

        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def user1 = new SecurityUser(username: 'sfercoci', password: 'cristian').addToRoles(adminRole).save(flush: true)


        service.springSecurityService = springSecurityServiceMock
        springSecurityServiceMock.getCurrentUser(_) >> user1
        shCommand = new ShCommand(command: "java -version")
                .addToRolesAllowed(adminRole)
                .save(flush: true)
        println(ShCommand.count)
    }


    void "can execute command"() {

        when:
        service.execute(shCommand)
        TimeUnit.MILLISECONDS.sleep(500)

        then:
        ShProcess.count == 1
        def process =  ShProcess.first()
        process.processNumber != null
        process.processNumber > 0
        !process.errOutput?.empty
    }

    void "can destroy process"() {
        given:
        service.execute(shCommand)
        def shProcess = ShProcess.first()
        assertTrue(shProcess != null)

        when:
        service.destroy(shProcess)

        then:
        ShProcess.count == 1
        def process =  ShProcess.first()
        process.processNumber != null
        process.processNumber > 0
        !process.errOutput?.empty
        !process.isRunning
    }

}
