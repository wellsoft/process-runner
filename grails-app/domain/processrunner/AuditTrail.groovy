package processrunner

class AuditTrail {

    Long id
    Date createdAt
    Date updatedAt

    static mapping ={
        table 'audit_trail'
        id generator: 'sequence', params: [sequence: 'audit_trail_seq']
        version false
        autoTimestamp: true
    }

/*    static marshalling = {
        shouldOutputClass false
        deep 'createdBy', 'updatedBy'
        serializer {
            createdAt Serializers.dateTimeToISO('createdAt')
            updatedAt Serializers.dateTimeToISO('updatedAt')
        }
    }*/
}
