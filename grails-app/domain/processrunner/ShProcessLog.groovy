package processrunner

class ShProcessLog {

    Long id
    String text

    static mapping ={
        table 'sh_process_log'
        version false
        id generator: 'sequence', params: [sequence: 'sh_process_log_seq']
    }

    static constraints = {
        text maxSize: 10000, blank: true, nullable: true
    }
}
