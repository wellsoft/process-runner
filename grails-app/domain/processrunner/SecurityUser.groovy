package processrunner

import org.apache.commons.lang.StringUtils
import org.springframework.security.core.userdetails.UserDetails

class SecurityUser{

	private static final long serialVersionUID = 1

	transient springSecurityService

	Long id
	String username
	String password
	Set<Role> roles
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
    static hasMany = [roles: Role]

    static mapping = {
        table 'security_user'
        version false
        id generator: 'sequence', params: [sequence: 'user_seq']
        roles joinTable: [name: 'user_role', column: 'role_id']
    }

	@Override
	int hashCode() {
		username?.hashCode() ?: 0
	}

	@Override
	boolean equals(other) {
		is(other) || (other instanceof SecurityUser && other.username == username)
	}

	@Override
	String toString() {
		username
	}

	Set<Role> getAuthorities() {
    		return roles
	}

    def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	def rolesToString () {
		return StringUtils.join(roles, ',')
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}

	static transients = ['springSecurityService', 'accountExpired', 'accountLocked', 'passwordExpired']

}
