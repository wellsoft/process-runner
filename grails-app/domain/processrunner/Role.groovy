package processrunner

class Role implements Serializable {

	private static final long serialVersionUID = 1

	Long id
	Set<SecurityUser> users
	String authority

	static belongsTo = [SecurityUser]
	static hasMany = [
			users: SecurityUser
	]

	static mapping = {
		table 'role'
		version false
		id generator: 'sequence', params: [sequence: 'role_seq']
		users joinTable: [name: 'user_role', column: 'user_id']
	}

	static constraints = {
		authority blank: false, unique: true
	}

	@Override
	int hashCode() {
		authority?.hashCode() ?: 0
	}

	@Override
	boolean equals(other) {
		is(other) || (other instanceof Role && other.authority == authority)
	}

	@Override
	String toString() {
		authority
	}
}
