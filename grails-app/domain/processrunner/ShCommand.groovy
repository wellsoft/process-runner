package processrunner

import org.apache.commons.lang.StringUtils

class ShCommand {

    Long id
    String command
    AuditTrail auditTrail
    Set<Role> rolesAllowed

    static belongsTo = [AuditTrail,Role]

    static hasMany = [rolesAllowed :Role]

    static mapping ={
        table 'sh_command'
        id generator: 'sequence', params: [sequence: 'sh_command_seq']
        rolesAllowed joinTable: [name: 'sh_command_role', key: 'sh_command_id', column: 'role_id'], cascade: 'none'
        version true
        auditTrail cascade: 'none'
        rolesAllowed cascade: 'none'
    }

/*    static marshalling = {
        shouldOutputClass false
        deep 'rolesAllowed', 'auditTrail'
        serializer {
            createdAt Serializers.dateTimeToISO('createdAt')
            updatedAt Serializers.dateTimeToISO('updatedAt')
        }
    }*/

     boolean isAllowed(Role role) {
        return rolesAllowed.contains(role)
    }

     String rolesToString(){
        return StringUtils.join(rolesAllowed, ',')
    }

}
