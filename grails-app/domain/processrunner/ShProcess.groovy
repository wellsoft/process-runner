package processrunner

/**
 * Proccess  domain that is linked by the process  lunched by the command .Can run only once 
 * @author kristian
 *
 */
class ShProcess {

    Long id
    SecurityUser user
    Integer processNumber //linux process number 
    ShCommand command
    Boolean isRunning = true
    AuditTrail auditTrail

    Set<ShProcessLog> errOutput
    Set<ShProcessLog> stdOutput

    static hasMany = [errOutput: ShProcessLog, stdOutput: ShProcessLog]
    static belongsTo = [ShCommand, SecurityUser, AuditTrail]
    static mapping = {
        table 'sh_process'
        id generator: 'sequence', params: [sequence: 'sh_process_seq']
        errOutput joinTable: [name: 'sh_process_err_output', key: 'sh_process_id', column: 'err_output_id'], cascade: 'none'
        stdOutput joinTable: [name: 'sh_process_std_output', key: 'sh_process_id', column: 'std_output_id'], cascade: 'none'
        version true
        user cascade: 'none'
    }

    public String logToString(errorLog) {
        def sortClosure = {it.id}
        def list
        if (errorLog) {
            list = errOutput.sort(sortClosure).collect { it.text + '\n' }
        } else {
            list = stdOutput.sort(sortClosure).collect { it.text + '\n' }
        }

        return list.join('\n')
    }
}

