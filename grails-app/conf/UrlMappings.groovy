class UrlMappings {

    static mappings = {

        '403'(controller: 'error', action: 'forbidden')
        '404'(controller: 'error', action: 'notFound')
        '500'(controller: 'error', action: 'serverError')

        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        group '/shCommand', {
            '/list' controller: 'shCommand', action: 'list'
            '/index' controller: 'shCommand', action: 'list'
            '/create' controller: 'shCommand', action: 'create'
            '/save' controller: 'shCommand', action: 'save'
            '/edit' controller: 'shCommand', action: 'edit'
            '/execute' controller: 'shCommand', action: 'execute'
        }

        group '/shProcess', {
            '/index' controller: 'shProcess', action: 'index'
            '/details' controller: 'shProcess', action: 'details'
            '/destroy' controller: 'shProcess', action: 'destroy'
            '/getLog/$id' controller: 'shProcess', action: 'getLog'
        }

        "/"(view: "/index")
    }
}
