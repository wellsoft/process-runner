import processrunner.Role
import processrunner.SecurityUser
import processrunner.SecurityUser
import processrunner.ShCommand

class BootStrap {

    def init = { servletContext ->

        if (Role.count == 0) {
            def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
            def userRole = new Role(authority: 'ROLE_USER').save(flush: true)
        }
        if (SecurityUser.count == 0) {

            def adminRole = Role.findByAuthority("ROLE_ADMIN")
            def userRole = Role.findByAuthority("ROLE_USER")

            def user1 = new SecurityUser(username: 'admin', password: 'admin1').addToRoles(adminRole).save(flush: true)

            def user2 = new SecurityUser(username: 'user', password: 'user1').addToRoles(userRole).save(flush: true)
        }

        if (ShCommand.count == 0) {
            new ShCommand(command: "java -version")
                    .addToRolesAllowed(Role?.findByAuthority("ROLE_ADMIN") ? Role?.findByAuthority("ROLE_ADMIN") : Role.first())
                    .save(flush: true)
        }

    }
    def destroy = {
    }
}
