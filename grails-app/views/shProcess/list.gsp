<html>
<head>
    <meta name="layout" content="main"/>
    <title>ShCommands</title>
</head>

<body>

<g:if test="${!records?.isEmpty()}">

    <table class="table-striped">
        <thead>
        <tr>
            <th class="span1">PID</th>
            <th>ShProcess</th>
            <th>createdAt</th>
            <th>createdBy</th>
            <th>Is running</th>
            <th></th>
            <sec:ifAnyGranted roles="ROLE_ADMIN">
                <th></th>
            </sec:ifAnyGranted>
        </tr>
        </thead>
        <tbody>
        <g:each in="${records}" var="shProcess" status="i">
            <tr>
                <td><strong>${shProcess.processNumber}</strong></td>
                <td>
                    <g:if test="${shProcess?.command}">
                        ${shProcess.command.command}
                    </g:if>
                    <g:else>
                        <small>(no name)</small>
                    </g:else>
                </td>
                <td>
                    ${shProcess.auditTrail?.createdAt}
                </td>
                <td>
                    ${shProcess.user?.username}
                </td>
                <td>
                    <strong>${shProcess.isRunning}</strong>
                </td>
                <td>
                    <g:link controller="shProcess" action="details" params="[id: shProcess.id]">details</g:link>
                </td>
                <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <g:if test="${shProcess.isRunning}">
                        <td>
                            <g:link controller="shProcess" action="destroy" params="[id: shProcess.id]">destroy</g:link>
                        </td>
                    </g:if>
                </sec:ifAnyGranted>
            </tr>
        </g:each>
        </tbody>
    </table>

</g:if>
<g:else>
    <div class="alert alert-warning">
        Empty list
        <br>
        <g:link controller="shCommand" action="list"> Back to ShCommand list</g:link>
    </div>
</g:else>

</body>
</html>
