<%--
  Created by IntelliJ IDEA.
  User: Grigoriy
  Date: 08.11.2015
  Time: 17:06
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Details of process ${shProcess.id}</title>

    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var errorLog = $('#error-log');

            var stdLog = $('#std-log');
            console.log(stdLog.html());
            var isRunningField = $('#is-running-field');

            function runLogRetrieving() {
                $.ajax({
                    url: "/ProcessRunner/shProcess/getLog/${shProcess.id}",
                    dataType: 'json',
                    success: function (data) {
                        errorLog.html(data.errorLog);
                        stdLog.html(data.stdLog);
                        if (data.isRunning == false) {
                            clearInterval(timerOnce);
                            isRunningField.html("FALSE");
                        }
                    },
                    error: function (request, status, error) {
                        errorLog.html('<h3>error retrieving error log</h3>');
                        stdLog.html('<h3>error retrieving std log</h3>');
                    }
                });
            }

            if ("${shProcess.isRunning}" == "true") {
                var timerOnce = setInterval(runLogRetrieving, 1000);
            }
        })
    </script>
</head>

<body>

<br>
<g:link controller="shProcess" action="index">< Back to ShProcess list</g:link>
<br>
<br>
<g:link controller="shCommand" action="index">< Back to ShCommand list</g:link>
<br>

<ol class="property-list    ">
    <li class="fieldcontain">
        <b>Process ID:</b> ${shProcess.processNumber}
    </li>
    <li class="fieldcontain">
        <b>Executed by:</b> ${shProcess.user}
    </li>
    <li class="fieldcontain">
        <b>ShCommand :</b> ${shProcess.command?.command}
    </li>
    <li class="fieldcontain">
        <b>ErrOutput:</b>

        <div class="row" id="error-log">
            ${raw(errOutput)}
        </div>
    </li>
    <li class="fieldcontain">
        <b>StdOutput:</b>

        <div class="row" id="std-log">
            ${raw(stdOutput)}
        </div>
    </li>
    <li class="fieldcontain">
        <b>Is running:</b> <span class="row" id="is-running-field">${shProcess.isRunning}</span>
    </li>
</ol>
</body>
</html>