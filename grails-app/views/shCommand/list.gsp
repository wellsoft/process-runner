<html>
<head>
    <meta name="layout" content="main"/>
    <title>ShCommands</title>
</head>

<body>
<sec:ifAnyGranted roles="ROLE_ADMIN">
    <g:link controller="shCommand" action="create" >Create new command</g:link>
</sec:ifAnyGranted>
<br/>
<g:if test="${!records?.isEmpty()}">
    <table class="table-striped">
        <thead>
        <tr>
            <th class="span1">№</th>
            <th>ShCommand</th>
            <th></th>
            <sec:ifAnyGranted roles="ROLE_ADMIN">
                <th>Roles Allowed</th>
                <th></th>
            </sec:ifAnyGranted>
        </tr>
        </thead>
        <tbody>
        <g:each in="${records}" var="shCommand" status="i">
            <tr>
                <td><strong>${i + 1}</strong></td>
                <td>
                    <g:if test="${shCommand?.command}">
                        ${shCommand.command}
                    </g:if>
                    <g:else>
                        <small>(no name)</small>
                    </g:else>
                </td>
                <td>
                    <g:link controller="shCommand" action="execute" params="[id: shCommand.id]">execute</g:link>
                </td>
                <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <td>
                        <div class="pull-right btn btn-success disabled">${shCommand.rolesToString()}</div>
                    </td>
                    <td>
                        <g:link controller="shCommand" action="edit" params="[id: shCommand.id]">edit</g:link>
                    </td>
                </sec:ifAnyGranted>

            </tr>
        </g:each>
        </tbody>
    </table>

</g:if>
<g:else>
    <div class="alert alert-warning">
        Empty list
    </div>
</g:else>

</body>
</html>
