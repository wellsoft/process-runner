<html>
<head>
    <meta name="layout" content="main"/>
    <title>Create ShCommand</title>
</head>

<body>

<g:link controller="shCommand" action="list"> Back to ShCommand list</g:link>

<g:form useToken="true" controller="shCommand" action="save" role="form" params="[id: shCommand.id]"
        class="form-horizontal form-decorated validation">
    <div class="row">
        Enter command
        <div>
            <textarea name="command" rows="3" required title="Enter command">${shCommand.command}</textarea>
        </div>
    </div>

    <div class="row">
        <select name="roles" multiple required>
            <g:each in="${rolesList}" var="role">
                <option value="${role.authority}" ${shCommand.rolesAllowed.contains(role) ? "selected" : ''}>${role.authority}</option>
            </g:each>
        </select>

    </div>

    <button type="submit" class="btn">Save</button>
</g:form>
</body>
</html>
