<html>
<head>
    <meta name="layout" content="main"/>
    <title>Create ShCommand</title>
</head>

<body>

<g:link controller="shCommand" action="list"> Back to ShCommand list</g:link>
<g:form useToken="true" controller="shCommand" action="save" role="form"
        class="form-horizontal form-decorated validation">
    <div class="row">
        Enter command
        <div>

        <textarea name="command" rows="3" required title="Enter command"></textarea>
        </div>
    </div>
    
    <div class="row">
        <g:select name="roles" from="${rolesList}" multiple="true"/>
    </div>

    <button type="submit" class="btn">Save</button>
</g:form>
</body>
</html>
