package processrunner

import grails.plugin.springsecurity.SpringSecurityService
import grails.transaction.Transactional
import org.apache.commons.lang.SystemUtils
import org.springframework.transaction.annotation.Propagation
import processrunner.concurrent.DaemonThreadFactory
import processrunner.concurrent.LogLoaderRunner

import java.util.concurrent.*

@Transactional
class ProcessExecutorService {

    public final String WIN_CMD = "cmd.exe"
    public final String WIN_DIR = "/c"
    private final String WIN_KILL = "taskkill /PID "

    private final String LINUX_CMD = "/bin/bash"
    private final String LINUX_DIR = "-c"
    private final String LINUX_KILL = "kill -9 "

    private final String terminal = SystemUtils.IS_OS_UNIX || SystemUtils.IS_OS_MAC ? LINUX_CMD : WIN_CMD
    private final String dir = SystemUtils.IS_OS_UNIX || SystemUtils.IS_OS_MAC ? LINUX_DIR : WIN_DIR
    private final String kill = SystemUtils.IS_OS_UNIX || SystemUtils.IS_OS_MAC ? LINUX_KILL : WIN_KILL

    SpringSecurityService springSecurityService

    static ScheduledFuture<?> t

    def execute(ShCommand shCommand) {
        ThreadFactory threadFactory = new DaemonThreadFactory()
        ExecutorService executor = Executors.newCachedThreadPool(threadFactory)
        ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor(threadFactory)
        ProcessBuilder processBuilder = new ProcessBuilder(terminal, dir, shCommand.command)

        SecurityUser user = springSecurityService.getCurrentUser()

        try {
            Process process = processBuilder.start()
            ShProcess shProcess = null
            ShProcess.withTransaction {
                shProcess = new ShProcess(
                        user: user,
                        processNumber: -99,
                        command: shCommand,
                )

                shProcess.processNumber = ProcessUtil.getProcessPID(process)
                shProcess.auditTrail = new AuditTrail(createdAt: new Date()).save()
                shProcess.save()
            }

            if (shProcess.id) {
                executor.execute(new LogLoaderRunner(process.getInputStream(), shProcess.id, false))
                executor.execute(new LogLoaderRunner(process.getErrorStream(), shProcess.id, true))
                t = scheduledExecutor.scheduleAtFixedRate(new CheckRunning(process, shProcess.id), 0, 1, TimeUnit.SECONDS)
            }
        } catch (IOException e) {
            log.error("Something bad", e)
        }
    }

    def destroy(ShProcess shProcess) {
        ProcessBuilder pb = new ProcessBuilder(terminal, dir, "${LINUX_KILL}${shProcess.processNumber}")
        try {
            Process process = pb.start()
            process.waitFor()

            shProcess.isRunning = false
            shProcess.save(flush: true)
        } catch (IOException e) {
            log.error("ERROR OCCURRED while destroying process", e)
        }
    }

    class CheckRunning implements Runnable {
        Process p
        Long shProcessId

        CheckRunning(Process p, Long shProcessId) {
            this.p = p
            this.shProcessId = shProcessId
        }

        void run() {
            if (!isRunning()) {
                ShProcess.withNewSession {
                    ShProcess.executeUpdate("update ShProcess sh set sh.isRunning = false where sh.id = ${shProcessId}")
                }
                t.cancel(false)
            }
        }

        boolean isRunning() {
            try {
                p.exitValue();
                return false;
            } catch (Exception ignore) {
                return true;
            }
        }
    }
}
