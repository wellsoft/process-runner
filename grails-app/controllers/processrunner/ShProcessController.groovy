package processrunner

import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import org.hibernate.sql.JoinType

import javax.servlet.http.HttpServletResponse

@Secured(['isAuthenticated()'])
class ShProcessController {
    SpringSecurityService springSecurityService
    ProcessExecutorService processExecutorService

    def index() {
        def records = ShProcess.createCriteria().listDistinct() {
            createAlias 'command', 'c', JoinType.LEFT_OUTER_JOIN
            createAlias 'user', 'u', JoinType.LEFT_OUTER_JOIN
            createAlias 'auditTrail', 'au', JoinType.LEFT_OUTER_JOIN

            order 'isRunning'
            order 'id', 'desc'
        }

        render(view: '/shProcess/list', model: [records: records])
    }


    def myHistory() {
        def user=springSecurityService.currentUser
        def records = ShProcess.createCriteria().listDistinct() {
            createAlias 'command', 'c', JoinType.LEFT_OUTER_JOIN
            createAlias 'user', 'u', JoinType.INNER_JOIN
            createAlias 'auditTrail', 'au', JoinType.LEFT_OUTER_JOIN

            order 'isRunning'
            order 'id', 'desc'
            eq 'user.id',user.id

        }

        render(view: '/shProcess/list', model: [records: records])
    }

    def listRunning() {

        def records = ShProcess.createCriteria().listDistinct() {
            createAlias 'command', 'c', JoinType.LEFT_OUTER_JOIN
            createAlias 'user', 'u', JoinType.INNER_JOIN
            createAlias 'auditTrail', 'au', JoinType.LEFT_OUTER_JOIN

            order 'isRunning'
            order 'id', 'desc'
            eq 'isRunning',true

        }

        render(view: '/shProcess/list', model: [records: records])
    }

    def details(Long id) {
        def shProcess = ShProcess.createCriteria().get {
            createAlias 'command', 'c', JoinType.LEFT_OUTER_JOIN
            createAlias 'user', 'u', JoinType.LEFT_OUTER_JOIN
            createAlias 'auditTrail', 'au', JoinType.LEFT_OUTER_JOIN
            createAlias 'errOutput', 'err', JoinType.LEFT_OUTER_JOIN
            createAlias 'stdOutput', 'std', JoinType.LEFT_OUTER_JOIN

            eq 'id', id
        }

        if (!shProcess) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND)
            return
        }

        def errOutput = shProcess.logToString(true)?.replaceAll("\r\n|\n\r|\n|\r", "<br/>")
        def stdOutput = shProcess.logToString(false)?.replaceAll("\r\n|\n\r|\n|\r", "<br/>")
        render(view: '/shProcess/details', model: [shProcess: shProcess, stdOutput: stdOutput, errOutput: errOutput])

    }

    def getLog(Long id) {
        def shProcess = ShProcess.createCriteria().get {
            createAlias 'command', 'c', JoinType.LEFT_OUTER_JOIN
            createAlias 'user', 'u', JoinType.LEFT_OUTER_JOIN
            createAlias 'auditTrail', 'au', JoinType.LEFT_OUTER_JOIN
            createAlias 'errOutput', 'err', JoinType.LEFT_OUTER_JOIN
            createAlias 'stdOutput', 'std', JoinType.LEFT_OUTER_JOIN

            eq 'id', id
        }
        if (!shProcess) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND)
            return
        }

        def payload = [:]
        payload.isRunning = shProcess.isRunning

        payload.success = true
        payload.errorLog = shProcess.logToString(true)?.replaceAll("\r\n|\n\r|\n|\r", "<br/>")
        payload.stdLog = shProcess.logToString(false)?.replaceAll("\r\n|\n\r|\n|\r", "<br/>")

        render(payload as JSON)
    }

    def destroy(Long id) {
        def shProcess = ShProcess.lock(id)
        if (!shProcess) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND)
            return
        }

        if (shProcess.processNumber < 0) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Process without PID")
            return
        }

        processExecutorService.destroy(shProcess)
        redirect(controller: 'shProcess', action: 'index')
    }

}
