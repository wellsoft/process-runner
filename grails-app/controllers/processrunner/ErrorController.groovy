package processrunner

class ErrorController {

  def badRequest() {
    render(text: 'BAD REQUEST')
  }

  def forbidden() {
    render(view: '403')
  }

  def notFound() {
    render(view: '404')
  }

  def serverError() {
    render(view: '500')
  }
}
