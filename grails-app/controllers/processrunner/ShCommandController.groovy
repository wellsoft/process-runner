package processrunner


import static grails.async.Promises.*

import grails.async.Promise
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import org.hibernate.sql.JoinType

import javax.servlet.http.HttpServletResponse

@Secured(['isAuthenticated()'])
class ShCommandController {

    SpringSecurityService springSecurityService
    ProcessExecutorService processExecutorService

    def list() {
        def isAdmin  = SpringSecurityUtils.ifAnyGranted("ROLE_ADMIN")
        def roles = springSecurityService.getCurrentUser().roles

        def records = ShCommand.createCriteria().listDistinct() {
            createAlias 'rolesAllowed', 'ra', JoinType.LEFT_OUTER_JOIN

            if(!isAdmin){
                or {
                    for (Role role : roles) {
                        eq 'ra.authority', role.authority
                    }
                }
            }

            order 'id', 'asc'
        }

        render(view: '/shCommand/list', model: [records: records])

    }

    @Secured(["hasRole('ROLE_ADMIN')"])
    def create() {

        render(view: '/shCommand/create', model: [rolesList: Role.list()])
    }

    @Secured(["hasRole('ROLE_ADMIN')"])
    def save() {
        withForm {
            ShCommand shCommand
            if (params.id) {
                shCommand = ShCommand.get(params.id as Long)
                if(shCommand.auditTrail){
                    shCommand.auditTrail.updatedAt = new Date()
                }

                def remove = []
                shCommand.rolesAllowed.each { Role role ->
                    if (!(params.roles instanceof String[])) {
                        if (params.roles != role.authority) {
                            remove << role
                        }
                    } else {
                        if (!params.roles.remove(role.authority)) {
                            remove << role
                        }
                    }
                }

                remove.each { Role role ->
                    shCommand.rolesAllowed.remove(role)
                }
            } else {
                shCommand = new ShCommand()
                shCommand.auditTrail = new AuditTrail(createdAt: new Date()).save(flush: true)
            }

            shCommand.command = params.command


            if (params.roles instanceof String[]) {
                    params.roles?.each { String authority ->
                    def role = Role.findByAuthority(authority)
                    if (!role) {
                        response.sendError(HttpServletResponse.SC_NOT_FOUND)
                        return
                    }
                    shCommand.addToRolesAllowed(role)
                }
            } else {
                def role = Role.findByAuthority(params.roles)
                if (!role) {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND)
                    return
                }
                shCommand.addToRolesAllowed(role)
            }

            shCommand.save(flush: true)

            redirect(controller: 'shCommand', action: 'list')
        }
    }

    def edit(Long id) {
        def shCommand = ShCommand.get(id)
        if (!shCommand) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND)
            return
        }

        render(view: '/shCommand/edit', model: [rolesList: Role.list(), shCommand: shCommand])
    }


    def execute(Long id) {
        def shCommand = ShCommand.get(id)
        if (!shCommand) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND)
            return
        }
        processExecutorService.execute(shCommand)
        redirect(controller: 'shProcess', action: 'listRunning')
    }

    def details(Long id) {
        // TODO show details view
    }
}
